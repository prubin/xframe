package xframe;



import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;

/**
 * CustomOutputStream redirects an output stream to a Swing text area.
 *
 * Source: http://www.codejava.net/java-se/swing/
 *                redirect-standard-output-streams-to-jtextarea
 * (with slight modifications).
 *
 * @author www.codejava.net
 * @author Paul A. Rubin (rubin@msu.edu)
 *
 */
public final class CustomOutputStream extends OutputStream {
  private final JTextArea textArea;   // the receptacle for the text
  private boolean dirty;              // is the container "dirty"

  /**
   * Constructor.
   * @param tArea the text area in which to display output.
   */
  public CustomOutputStream(final JTextArea tArea) {
    textArea = tArea;
    dirty = false;
  }

  /**
   * Write a byte.
   * @param b the byte to write
   * @throws IOException if the write fails
   */
  @Override
  public void write(final int b) throws IOException {
    // Redirect data to the text area.
    textArea.append(String.valueOf((char) b));
    // Scroll the text area to the end of data.
    textArea.setCaretPosition(textArea.getDocument().getLength());
    // Mark the contents "dirty".
    dirty = true;
  }

  /**
   * Write the contents to a file.
   * @param target the destination file
   * @throws IOException if the write fails for any reason
   */
  public void save(final File target) throws IOException {
    try (FileWriter fw = new FileWriter(target)) {
      textArea.write(fw);
      dirty = false;
    }
  }

  /**
   * Check whether the window is marked dirty.
   * @return true if there is unsaved content
   */
  public boolean isDirty() {
    return dirty;
  }

  /**
   * CLear the contents.
   */
  public void clear() {
    textArea.setText(null);
    dirty = false;
  }
}

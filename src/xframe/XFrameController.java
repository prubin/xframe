package xframe;

import java.io.File;

/**
 * XFrameController defines the interface used by program logic to manage
 * an instance of the XFrame GUI.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public interface XFrameController {

  /**
   * Returns the current directory (the starting point for any file chooser
   * dialog).
   * @return the current directory
   */
  File getCurrentDirectory();

  /**
   * Sets the current directory.
   * @param target the new current directory
   */
  void setCurrentDirectory(final File target);

  /**
   * Signals the program to perform a computational task that will run in
   * the background (so as not to interfere with the user interface).
   * @return a string report on the results of the computation (to be displayed
   * in the GUI)
   * @throws Exception (any exception the computation task might throw)
   */
  String runComputations() throws Exception;

  /**
   * Instructs the controller to abort the background task.
   * @return true if the task was successfully aborted
   */
  boolean abortTask();
}

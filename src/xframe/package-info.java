/**
 * A customized Swing JFrame designed to be the starting point for experiment
 * platforms.
 *
 * {@link XFrame} is a tailored JFrame that provides the following:
 * <ul>
 * <li>a central text area (scrollable) to capture program output, with controls
 * to save or clear the content;</li>
 * <li>a status label at the bottom center, with getter and setter;</li>
 * <li>getter and setter for the window title;</li>
 * <li>a nested subclass of SwingWorker to use for running background
 * computations;</li>
 * <li>a modal dialog for the program to issue warnings or error messages;</li>
 * <li>a scrollable nonmodal text window;</li>
 * <li>built-in protection against accidental deletion of the text area
 * content.</li>
 * </ul>
 * <p>
 * {@link XFrameController} defines an interface that the program must
 * implement to allow an XFrame to communicate with it.
 * <p>
 * {@link CustomOutputStream} provides a mechanism for the program to redirect
 * output streams (including stdout and stderr) to the XFrame text area.
 */
package xframe;

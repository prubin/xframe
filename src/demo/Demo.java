package demo;

/**
 * Demo sets up the GUI and controller classes for a simple demonstration of
 * the XFrame class.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Demo {

  /**
   * Constructor (never used).
   */
  private Demo() { }

  /**
   * Start the demonstration.
   * @param args the command line arguments (ignored)
   */
  public static void main(final String[] args) {
    // Create a GUI window.
    DemoFrame frame = new DemoFrame();
    // Create a controller.
    DemoController controller = new DemoController(frame);
    // Set the controller for the XFrame.
    frame.setController(controller);
  }

}

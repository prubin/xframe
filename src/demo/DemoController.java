package demo;

import java.io.File;
import java.util.concurrent.TimeUnit;
import xframe.XFrameController;

/**
 * DemoController contains the (rather minimal) program logic for a
 * demonstration of the XFrame class.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class DemoController implements XFrameController {
  private static final int REPETITIONS = 20;
    // number of times to print the message
  private static final String MESSAGE = "Hello world!";
    // message to print
  private static final int PAUSE = 250;
    // how many milliseconds to pause between prints

  private final DemoFrame demoFrame;  // the GUI for the demonstration
  private int iterationCount;         // how many times the task loop executes
  private boolean isAborted;          // has the task been aborted?
  private File currentDirectory;      // starting directory for file operations

  /**
   * Constructor.
   * @param frame the GUI frame
   */
  public DemoController(final DemoFrame frame) {
    demoFrame = frame;
    // Divert standard output to the frame's message area.
    System.setOut(demoFrame.getCustomStream());
    // Set the starting directory to the user's home directory.
    currentDirectory = new File(System.getProperty("user.home"));
    // Set the initial status message to "idle".
    demoFrame.setStatusMessage("idle");
    // Set the window title.
    demoFrame.setTitle("XFrame Demonstration");
    // Show some text in a separate window (just to show it can be done).
    demoFrame.showScrollableText(
       "Miscellaneous Blather",
       "<ul><li>This demonstrates a feature that allows the program to display "
         + "text in a separate window from the main window.</li>"
         + "<li>Options allow you to:"
         + "<li>specify that the text is HTML (versus plain text) and</li>"
         + "<li>include a button letting the user save the text to a file."
         + "</li></ul><p>Try <b>Run > Run demo</b> to see a background task in "
         + "action. (It just prints '" + MESSAGE + "' " + REPETITIONS
         + " times.)<p>There is a separate function that lets you pop up"
         + " information/warning messages. To see this in action, run the"
         + " background task and then click the <b>Run > Abort demo</b> menu"
         + " option before the background task completes.<p>Note that you can"
         + " leave this window open while using the main window.",
       true,
       true);
  }

  /**
   * Returns the current directory (the starting point for any file chooser
   * dialog).
   * @return the current directory
   */
  @Override
  public File getCurrentDirectory() {
    return currentDirectory;
  }

  /**
   * Sets the current directory.
   * @param target the new current directory
   */
  @Override
  public void setCurrentDirectory(final File target) {
    currentDirectory = target;
  }

  /**
   * Performs the demonstration task in the background.
   * In this case, it just prints a single string 10 times or until aborted.
   * @return a string report on the results (how many times the string was
   * printed)
   * @throws Exception (any exception the background task might throw, which
   * in this case will hopefully be none)
   */
  @Override
  public String runComputations() throws Exception {
    isAborted = false;
    iterationCount = 0;
    // Set the window status message.
    demoFrame.setStatusMessage("... running ...");
    while (!isAborted && iterationCount < REPETITIONS) {
      // Sleep one second to manage the output rate.
      TimeUnit.MILLISECONDS.sleep(PAUSE);
      System.out.println(MESSAGE);
      iterationCount += 1;
    }
    // Reset the status message.
    demoFrame.setStatusMessage("idle");
    return "\nThe message printed " + iterationCount + " times.\n";
  }

  /**
   * Instructs the controller to abort the background task.
   * @return true if the task was successfully aborted
   */
  @Override
  public boolean abortTask() {
    // Set the abort flag.
    isAborted = true;
    // Warn the user that the task has been aborted (just to demonstrate the
    // use of the warn() method).
    demoFrame.warn("Aborted", "The background task has been aborted.", false);
    // Optimistically signal success.
    return true;
  }
}

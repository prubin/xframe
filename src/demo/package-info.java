/**
 * A demonstration of XFrame.
 *
 * The demo program uses a background process to write a message multiple times
 * in the main text area, and then prints the number of repetitions. It also
 * opens a nonmodal window, to demonstrate that feature.
 * <p>
 * Use <code>Run &gt; Run demo</code> to execute
 * the demonstration.
 * <p>
 * To see a warning message, run the demonstration and, before it completes,
 * click <code>Run &gt; Abort demo</code>.
 * <p>
 * {@link Demo} creates a controller and an XFrame window and does nothing else.
 * <p>
 * {@link DemoFrame} is an XFrame customized for the demonstration.
 * <p>
 * {@link DemoController} implements the XFrameController interface and contains
 * the program logic.
 */
package demo;

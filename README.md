#XFrame
**XFrame** is a Swing JFrame with some added functionality. I created it to be a starting point for writing Swing applications to do computational experiments of various sorts. It contains a few common elements I like to have in all my programs.
## What XFrame has
**XFrame** contains the following features:

- The `File > Exit` menu item (the only menu item the class starts with) and the window close button (typically an "x" in the upper right corner) will ask the user to confirm the close operation before the program exits.
- The central portion of the window is a scrollable text area, where the program can write any sort of output. It employs a customized output stream (the `CustomOutputStream` class) to funnel text into the window. The program can get the stream using `XFrame.getCustomStream()` and then pass it to `System.setOut()` or `System.setErr()` to route output from print statement or error messages to the window.
-- The text area has **Save** and **Clear** buttons. The former lets the user save the window content to a file; the latter clears the content.
-- `XFrame` tracks whether there is unsaved content in the window and will ask the user to confirm any attempt to clear it or to close the window when there is unsaved text.
- The bottom of the screen contains an area (between the **Save** and **Clear** buttons) for status messages. There are methods to get the current status message and to set a new one.
- There are methods to get the current window title and to set a new one.
- The `XFrame.warn()` method provides an easy mechanism for the program to pop up modal warning or error dialogs.
- The `XFrame.showScrollableText()` method lets the program display ordinary text or HTML content in a scrollable nonmodal window. It has an option that adds a **Save** button that will save the contents to a file.
- There is a built-in subclass (`ComputationTask`) that can be used for running tasks in a background thread (to avoid interfering with the main window). It uses a method in the controller interface (`XFrameController.runComputations()`) to tell the program to run the task (and to get a String message with the results, such as a final status). Currently, it only supports one background task at a time, and only one type of background task, but in the future I may add the ability to have multiple distinct tasks running in separate `SwingWorker` instances.
## Demonstration
The `demo` package contains a modified version of `XFrame` (`DemoFrame`) and a simple implementation of `XFrameController` (`DemoController`) that shows off the built in features. The demonstration is just a variant of the standard "Hello world" program. When run, it writes a message multiple times in the central display area and then shows the number of times the message printed. To see the `warn()` method in action, use `Run > Run demo` to start the demonstration and then `Run > Abort demo` to abort it. 
## Screenshots
These screenshots are from the demonstration program.
#### Main Window
![main window](./main_window.png  "Main window")
#### Nonmodal Dialog
![nonmodal dialog](./dialog_window.png "Nonmodal dialog")
## How to use it
**XFrame** itself consists of two classes and an interface, all contained in the `xframe` package. Add those to your project and then modify them as follows.

- `XFrame` is the actual GUI piece (an extension of `JFrame`. You can modify it as you would any JFrame, adding menus any needed functionality.
-- In particular, you may also want to add code to the `changeState()` method, which is called when the main window is first displayed and when any background task starts, ends or is aborted. (You may choose to call it from other places as well.) The `changeState()` method is a handy place to put logic that selects which menu items are enabled or disabled at any time, so that you do not have a File > Save menu item enabled when there is no file to save, or an Abort menu item enabled when there is nothign to abort.
- `CustomOutputStream` handles the funneling of program output to the central text area. It is unlikely you will need to modify it.
- `XFrameController` defines an interface used by `XFrame` to communicate with the program. You need to implement the interface somewhere in your code. Also, you need to invoke `XFrame.setController()` on the `XFrame` instance immediately after creating the instance, to link the controller to the window.

